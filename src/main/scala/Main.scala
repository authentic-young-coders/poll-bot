import scala.io.Source.fromFile
import scala.util.chaining._
import scala.util.Random
import scala.jdk.OptionConverters._
import collection.JavaConverters._

import org.javacord.api.DiscordApiBuilder
import org.javacord.api.interaction.SlashCommand
import org.javacord.api.interaction.SlashCommandOption
import org.javacord.api.interaction.SlashCommandOptionType as SlashType

import domain.{Raffles, RaffleId}
import domain.RaffleCreateException
import domain.RaffleLookupException
import java.util.stream.Collectors
import org.javacord.api.entity.message.embed.EmbedBuilder
import org.javacord.api.interaction.callback.InteractionImmediateResponseBuilder
import org.javacord.api.interaction.SlashCommandInteraction
import org.javacord.api.interaction.SlashCommandInteractionOptionsProvider
import org.javacord.api.DiscordApi
import scala.collection.mutable.ListBuffer

def attempt[A](a: A) = try { a } catch { case e: Exception => {} }

extension (interaction: SlashCommandInteractionOptionsProvider)
  def options = interaction.getOptions.asScala
  def firstOption = interaction.getFirstOption.asScala
  def firstOptionString = interaction.getFirstOptionStringValue.asScala
  def secondOptionString = interaction.getSecondOptionStringValue.asScala

extension (interaction: SlashCommandInteraction)
  def server = interaction.getServer.toScala

extension [T](xs: List[T])
  def repeat(n: Int): List[T] = List.fill(n)(xs).flatten
  def random: Option[T] = Random.shuffle(xs).headOption
@main def main: Unit = 
  val token = fromFile("data/token").mkString
  val api = DiscordApiBuilder().setToken(token).setAllIntents().login().join()

  api.getGlobalSlashCommands.join.forEach(cmd => 
    attempt(cmd.deleteGlobal.join)
    api.getServers.asScala.foreach(server =>
      attempt(cmd.deleteForServer(server).join)
    )
  )
  val serverCommands = api.getServers.asScala.filter(_.getId == 887412711808438333L).map(Commands(api, _))
  println(serverCommands.size)

  api.addSlashCommandCreateListener(event => 
    implicit val interaction = event.getSlashCommandInteraction
    val responder = interaction.createImmediateResponder

    serverCommands.foreach(implicit commands => 
      val res: Option[InteractionImmediateResponseBuilder] = interaction.getCommandId match
        case commands.raffleId =>
          for
            subCommand <- interaction.firstOption
//            _ <- interaction.getFirstOptio
            serverId   <- interaction.server.map(_.getId)
            result     <- subCommand.getName match
              case "add"  => addRaffle(subCommand.firstOptionString).map(responder.setContent(_))
              case "run"  => runRaffle(subCommand.firstOptionString, subCommand.getSecondOptionIntValue.asScala, responder, api)
              case "show" => showRaffle(subCommand.firstOptionString, responder)
              case _ => Some(responder.setContent("UNPROCESSED RAFFLE"))
          yield result

        case commands.userId =>
          for
            subCommand <- interaction.firstOption
            serverId   <- interaction.server.map(_.getId)
            result     <- subCommand.getName match
              case "add" =>
                val result = for
                  user       <- subCommand.getFirstOptionUserValue.toScala
                  raffleName <- subCommand.secondOptionString
                yield Raffles.addUser(RaffleId(serverId, raffleName), user.getId)
                val error = RaffleLookupException.NotFound

                result.map(_ match
                  case Right(())   => "User successfully added!"
                  case Left(error) => "Raffle not found"
                ).map(responder.setContent(_))
          yield result
   
        // case commands.showRaffleId => showRaffle(interaction.firstOptionString, responder)

        // case commands.addRaffleId =>
        //   addRaffle(interaction.firstOptionString).map(responder.setContent(_))

        // case commands.addUserId =>
        //   val result = for
        //     user       <- interaction.getFirstOptionUserValue.toScala
        //     raffleName <- interaction.getSecondOptionStringValue.toScala
        //     serverId   <- interaction.getServer.map(_.getId).toScala
        //   yield Raffles.addUser(RaffleId(serverId, raffleName), user.getId)
        //   val error = RaffleLookupException.NotFound

        //   result.map(_ match
        //     case Right(())   => "User successfully added!"
        //     case Left(error) => "Raffle not found"
        //   ).map(responder.setContent(_))

        case commands.pingId => Some(responder.setContent("Pong!"))

        case other => Some(responder.setContent("NOT PROCESSED"))
      res.getOrElse(responder.setContent("Missing some value (are you in a server?)")).respond()
    )
  )

  println("Started!")

def addRaffle(name: Option[String])(implicit interaction: SlashCommandInteraction, commands: Commands) =
  for
    name     <- name
    serverId <- interaction.server.map(_.getId)
  yield 
    Raffles.create(RaffleId(serverId, name)).tap(_ => commands.updateAddUsersCommandWithRaffles) match
      case Right(raffle) => "Raffle successfully created!"
      case Left(RaffleCreateException.AlreadyExists) => "Raffle already exists"

def runRaffle(name: Option[String], winners: Option[Integer], responder: InteractionImmediateResponseBuilder, api: DiscordApi)(
  implicit interaction: SlashCommandInteraction, commands: Commands
) =
  for
    name     <- name
    winners  <- winners
    server   <- interaction.server
    raffle   <- Raffles.getAll.find(r => RaffleId(server.getId, name) == r.id)
  yield 
    val names = raffle.userIds.map(id =>
      val user = api.getUserById(id).join
      user.getNickname(server).orElse(user.getName)
    )
    val mentionedNames = raffle.userIds.map(id => s"<@${id}>")

    val thread = new Thread {
      override def run = 
        Thread.sleep(5000)
        val names = ListBuffer[String]()
        val name = mentionedNames.random.getOrElse("Must have names in the raffle")
        names.addOne(name)

        val message = interaction.createFollowupMessageBuilder.setContent(s":star: ${name} :star:").send.join
        def f(currentWinner: Int): Unit =
          val name = mentionedNames.filter(n => !names.contains(n)).random.getOrElse("OUT OF NAMES")
          names.addOne(name)
          if (currentWinner < winners)
            Thread.sleep(1500)
            message.edit(s"${message.getContent}\n:star: ${name} :star:").join
            f(currentWinner + 1)
        f(1)


        // def g(currentWinner: Int): Unit =
        //   val message = interaction.createFollowupMessageBuilder.setContent(s":star: ${mentionedNames.random} :star:").send.join
        //   if (currentWinner < winners)
        //     def f(count: Int, time: Int): Unit =
        //       Thread.sleep(time)
        //       if (count == 4)
        //         message.edit(s"${message.getContent}\n:star: ${mentionedNames.random} :star:").join
        //       else
        //         message.edit(names.random).join
        //         f(count + 1, time)
        //     f(4, 1500)
        //   g(currentWinner + 1)
        // g(0)
    }
    thread.start

    responder.setContent(s"Starting raffle shuffle in 5 seconds! ${winners} winners will be selected.")

def showRaffle(name: Option[String], responder: InteractionImmediateResponseBuilder)(
  implicit interaction: SlashCommandInteraction, commands: Commands
) =
  for
    name     <- name
    serverId <- interaction.server.map(_.getId)
    raffle   <- Raffles.getAll.find(r => RaffleId(serverId, name) == r.id)
  yield 
    val (u1, u2) = raffle.userIds.map(id => s"<@$id>").pipe(xs => xs.splitAt(xs.size / 2))
    val embed = EmbedBuilder()
      .setTitle(raffle.id.name)
      .addInlineField("\u200b", u1.mkString("\n"))
      .addInlineField("\u200b", u2.mkString("\n"))
    responder.addEmbed(embed) 
