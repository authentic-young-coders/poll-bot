import org.javacord.api.interaction.SlashCommand
import org.javacord.api.interaction.SlashCommandOption
import org.javacord.api.interaction.SlashCommandOptionType as SlashType
import org.javacord.api.DiscordApi

import scala.util.chaining._

import scala.collection.JavaConverters._
import domain.Raffles
import org.javacord.api.interaction.SlashCommandOptionChoice
import org.javacord.api.interaction.SlashCommandUpdater
import org.javacord.api.entity.server.Server

class Commands(api: DiscordApi, val server: Server):
  val (ping, pingId) = create("ping", "Testing", List())
  val (raffle, raffleId) = create("raffle", "Manage raffles", List(
    SlashCommandOption.createWithOptions(SlashType.SUB_COMMAND, "add", "Add a raffle", List(
      SlashCommandOption.create(SlashType.STRING, "NAME", "The name of the raffle", true)
    )),
    SlashCommandOption.createWithOptions(SlashType.SUB_COMMAND, "show", "Show a raffle", List(raffleOptions("The raffle to show"))),
    SlashCommandOption.createWithOptions(SlashType.SUB_COMMAND, "run", "Find a winner!", List(
      raffleOptions("The raffle to run"),
      SlashCommandOption.create(SlashType.INTEGER, "WINNERS", "The number of winners", true)
    ))
  ))

  val (user, userId) = create("user", "Manage users", List(
    SlashCommandOption.createWithOptions(SlashType.SUB_COMMAND, "add", "Add a user", addUserOptions)
  ))
  // val (addRaffle, addRaffleId) = create("add_raffle", "Add a raffle", List(
  //   SlashCommandOption.create(SlashType.STRING, "NAME", "The name of the raffle", true),
  // ))

  // val (show_raffle, showRaffleId) = create("show_raffle", "Manage raffles", List(
  //   raffleOptions("The raffle to show")
  // ))

  // val (addUser, addUserId) = create("add_user", "Add user to a raffle", addUserOptions)

  def updateAddUsersCommandWithRaffles: Unit =
    SlashCommandUpdater(raffleId).setSlashCommandOptions(List(
      SlashCommandOption.createWithOptions(SlashType.SUB_COMMAND, "add", "Add a raffle", List(
        SlashCommandOption.create(SlashType.STRING, "NAME", "The name of the raffle", true)
      )),
      SlashCommandOption.createWithOptions(SlashType.SUB_COMMAND, "show", "Show a raffle", List(raffleOptions("The raffle to show"))),
      SlashCommandOption.createWithOptions(SlashType.SUB_COMMAND, "run", "Find a winner!", List(
        raffleOptions("The raffle to run"),
        SlashCommandOption.create(SlashType.INTEGER, "WINNERS", "The number of winners", true)
      ))
    )).updateForServer(server).join()

    SlashCommandUpdater(userId).setSlashCommandOptions(List(
      SlashCommandOption.createWithOptions(SlashType.SUB_COMMAND, "add", "Add a user", addUserOptions)
    )).updateForServer(server).join()
    // SlashCommandUpdater(addUserId).setSlashCommandOptions(addUserOptions).updateForServer(server).join()
    // SlashCommandUpdater(showRaffleId).setSlashCommandOptions(List(raffleOptions("The raffle to show"))).updateForServer(server).join()

  private def addUserOptions = 
    List(
      SlashCommandOption.create(SlashType.USER, "USER", "The user to add to the raffle", true), 
      raffleOptions("The raffle to add the user to")
    )

  private def raffleOptions(description: String) =
      SlashCommandOption.createWithChoices(SlashType.STRING, "RAFFLE", description, true, 
        Raffles.getAll.map(r => SlashCommandOptionChoice.create(r.id.name, r.id.name))
      )

  private def create(
    name: String, 
    description: String, 
    options: List[SlashCommandOption]
  ): (SlashCommand, Long) = 
    val command = (options match
      case Nil => SlashCommand.`with`(name, description)
      case xs  => SlashCommand.`with`(name, description, options)
    ).createForServer(server).join()

    (command, command.getId)

implicit def listToJava[T](xs: List[T]): java.util.List[T] = xs.asJava
