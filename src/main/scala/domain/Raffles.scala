package domain

import spray.json._
import DefaultJsonProtocol._

import os._
import scala.util.chaining._

object Raffles:
  def create(id: RaffleId): Either[RaffleCreateException, Raffle] =
    if (get(id).isEmpty)
      Right(Raffle(id, List()).tap(r => set(getAll :+ r)))
    else
      Left(RaffleCreateException.AlreadyExists)

  def addUser(id: RaffleId, userId: Long): Either[RaffleLookupException, Unit] =
    var found = false
    set(getAll.map(raffle => 
      if (raffle.id == id) 
        found = true
        raffle.copy(userIds = raffle.userIds :+ userId) 
      else 
        raffle
    ))

    if (found) Right(()) else Left(RaffleLookupException.NotFound)

  def get(id: RaffleId): Option[Raffle] =
    getAll.find(r => r.id == id)

  def getAll: List[Raffle] = 
    os.read(file).parseJson.convertTo[List[Raffle]]

  private def file = os.pwd/"data"/"raffles.json"

  private def set(raffles: List[Raffle]): Unit =
    os.write.over(file, raffles.toJson.prettyPrint)

enum RaffleCreateException:
  case AlreadyExists

enum RaffleLookupException:
  case NotFound
  
case class RaffleId(serverId: Long, name: String)
implicit val raffleIdFormat: RootJsonFormat[RaffleId] = jsonFormat2(RaffleId.apply)

case class Raffle(id: RaffleId, userIds: List[Long])
implicit val raffleFormat: RootJsonFormat[Raffle] = jsonFormat2(Raffle.apply)
