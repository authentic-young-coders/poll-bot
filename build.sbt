val scala3Version = "3.1.0"

lazy val root = project
  .in(file("."))
  .settings(
    name := "raffle_bot",
    version := "0.1.0-SNAPSHOT",

    scalaVersion := scala3Version,

    libraryDependencies ++= Seq(
      "org.javacord" % "javacord" % "3.3.2",
      ("io.spray" %% "spray-json" %"1.3.6").cross(CrossVersion.for3Use2_13),
      // "com.discord4j" % "discord4j-core" % "3.2.0",
      "com.lihaoyi" %% "os-lib" % "0.7.8",
      "com.novocode" % "junit-interface" % "0.11" % "test"
    )
  )
